# ESSLLI 2017 Course: Introduction to Categorical Compositional Distributional Semantics

This repository contains the slides of the ESSLLI 2017 introductory course on the emerging field of _categorical compositional distributional semantics_ (informally referred to as the _DisCo_ model). Inspired by quantum protocols, the DisCo model has provided a convincing account of compositionality in vector space models of NLP, unifying the two orthogonal paradigms of formal semantics and distributional models of meaning. The resulting setting has  systematically extended the vector models from words to sentences, enabling them to reason about sentence meaning with the same tools as for word meaning. Based on the rigid mathematical framework of compact closed categories, the model has made possible novel approaches in language-related problems, and allowed the theoretical study of compositional aspects in distributional models of meaning. The course is designed to provide a comprehensive introduction of the field to students and researchers, covering  mathematical and linguistic foundations, past and current research, and discussing advanced topics and open problems. 

## Structure

* **Lecture 1:** Introduction
* **Lecture 2:** Categorical Compositional Distributional Semantics
* **Lecture 3:** Functional Words and Frobenius Algebras
* **Lecture 4:** Using Density Matrices for Ambiguity and Entailment
* **Lecture 5:** Advanced Topics: Conceptual Spaces, Double Density Matrices, Logic

## References

B. Coecke, M. Sadrzadeh, S. Clark (2010). **Mathematical Foundations for a Compositional Distributional Model of Meaning.** _Lambek Festschrift. Linguistic Analysis_, 36:345-384.
 
E. Grefenstette, M. Sadrzadeh (2011). **Experimental support for a categorical compositional distributional model of meaning**. In _Proceedings of the 2011 Conference on Empirical Methods in Natural Language Processing_, pages 1394-1404, Edinburgh, Scotland

D. Kartsaklis, M. Sadrzadeh, S. Pulman, and B. Coecke (2016). **Reasoning about meaning in natural language with compact closed categories and Frobenius algebras.** In J. Chubb, A. Eskandarian, and V. Harizanov, editors, _Logic and Algebraic Structures in Quantum Computing_, pages 199-222. Cambridge University Press

R. Piedeleu, D. Kartsaklis, B. Coecke, and M. Sadrzadeh (2015). **Open System Categorical Quantum Semantics in Natural Language Processing**. In _Proceedings of the 6th Conference on Algebra and Coalgebra in Computer Science (CALCO)_, Nijmegen, Netherlands
